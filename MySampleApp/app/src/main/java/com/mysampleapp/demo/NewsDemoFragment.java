package com.mysampleapp.demo;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.mysampleapp.R;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

public class NewsDemoFragment extends DemoFragmentBase {

    String[] content_titles = {"1", "2", "3", "4"};

    private static final Charset CHARSET_UTF8 =
            Charset.forName("UTF-8");

    private static final CharsetEncoder ENCODER = CHARSET_UTF8.newEncoder();
    private static final CharsetDecoder DECODER = CHARSET_UTF8.newDecoder();

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {

        Log.d(NewsDemoFragment.class.getSimpleName(), "NewsDemoFragment.onCreateView called");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_demo_home, container, false);
    }


    private void invokeFunction() {

        final String functionName = "maternanewsapp-dev-getNews";
        final String requestPayload = " ";


        new AsyncTask<Void, Void, InvokeResult>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
            }

            @Override
            protected void onCancelled(InvokeResult invokeResult) {
                super.onCancelled(invokeResult);
            }

            @Override
            protected InvokeResult doInBackground(Void... params) {
                try {
                    Log.e(NewsDemoFragment.class.getSimpleName(), "AWS Lambda invocation started");
                    final ByteBuffer payload =
                            ENCODER.encode(CharBuffer.wrap(requestPayload));

                    final InvokeRequest invokeRequest =
                            new InvokeRequest()
                                    .withFunctionName(functionName)
                                    .withInvocationType(InvocationType.RequestResponse)
                                    .withPayload(payload);

                    Log.e(NewsDemoFragment.class.getSimpleName(),
                            invokeRequest.getFunctionName() + " "
                    );

                    final InvokeResult invokeResult =
                            AWSMobileClient
                                    .defaultMobileClient()
                                    .getCloudFunctionClient()
                                    .invoke(invokeRequest);

                    Log.e(NewsDemoFragment.class.getSimpleName(), "invokeResult StatusCode: " + invokeResult.getStatusCode());
                    Log.e(NewsDemoFragment.class.getSimpleName(), "invokeResult FunctionError: " + invokeResult.getFunctionError());

                    return invokeResult;
                } catch (final Exception e) {
                    Log.e(NewsDemoFragment.class.getSimpleName(), "AWS Lambda invocation failed : " + e.getMessage(), e);
                    final InvokeResult result = new InvokeResult();
                    result.setStatusCode(500);
                    result.setFunctionError(e.getMessage());
                    return result;
                }
            }

            @Override
            protected void onPostExecute(final InvokeResult invokeResult) {

                try {
                    final int statusCode = invokeResult.getStatusCode();
                    final String functionError = invokeResult.getFunctionError();
                    final String logResult = invokeResult.getLogResult();

                    if (statusCode != 200) {
                        Log.e(NewsDemoFragment.class.getSimpleName(), invokeResult.getFunctionError());
                    } else {
                        final ByteBuffer resultPayloadBuffer = invokeResult.getPayload();
                        final String resultPayload = DECODER.decode(resultPayloadBuffer).toString();
                        parseResult(resultPayload);
                    }

                    if (functionError != null) {
                        Log.e(NewsDemoFragment.class.getSimpleName(), "AWS Lambda Function Error: " + functionError);
                    }

                    if (logResult != null) {
                        Log.d(NewsDemoFragment.class.getSimpleName(), "AWS Lambda Log Result: " + logResult);
                    }
                }
                catch (final Exception e) {
                    Log.e(NewsDemoFragment.class.getSimpleName(), "Unable to decode results. " + e.getMessage(), e);
                    Log.e(NewsDemoFragment.class.getSimpleName(),e.getMessage());
                }
            }
        }.execute();
    }

    private void parseResult(String resultPayload) {
        Log.e(NewsDemoFragment.class.getSimpleName(), resultPayload);

    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        invokeFunction();

        final NewsAdapter adapter = new NewsAdapter(getActivity());
        adapter.addAll(content_titles);
        ListView listView = (ListView) view.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view,
                                    final int position, final long id) {
                //item clicked
            }
        });
    }

    private static final class NewsAdapter extends ArrayAdapter<String> {

        private LayoutInflater inflater;



        public NewsAdapter(final Context context) {
            super(context, R.layout.list_item_icon_text_with_subtitle_no_icon);
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view;
            ViewHolder holder;
            if (convertView == null) {
                view = inflater.inflate(R.layout.list_item_icon_text_with_subtitle_no_icon, parent, false);
                holder = new ViewHolder();
                holder.titleTextView = (TextView) view.findViewById(R.id.list_item_title);
                holder.subtitleTextView = (TextView) view.findViewById(R.id.list_item_subtitle);
                view.setTag(holder);
            } else {
                view = convertView;
                holder = (ViewHolder) convertView.getTag();
            }

            holder.titleTextView.setText(this.getItem(position));
            holder.subtitleTextView.setText(this.getItem(position));

            return view;
        }
    }






    private static final class ViewHolder {
        TextView titleTextView;
        TextView subtitleTextView;
    }
}
