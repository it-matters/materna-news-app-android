package de.materna.maternanews.util;


import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

/**
 * Utility class for L APIs
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class LUtils {

    protected AppCompatActivity mActivity;

    private LUtils(AppCompatActivity activity) {
        mActivity = activity;
    }

    public static LUtils getInstance(@NonNull AppCompatActivity activity) {
        return new LUtils(activity);
    }

    private static boolean hasL() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    /**
     * Returns the color of the status bar. On pre-L devices, you can have any status bar color
     * so long as it's black.
     * @return The color of the status bar.
     */
    @ColorInt
    public int getStatusBarColor() {
        return !hasL() ? Color.BLACK : mActivity.getWindow().getStatusBarColor();
    }

    /**
     * Sets the color of the status bar to color.
     * @param color The color being set.
     */
    public void setStatusBarColor(@ColorInt int color) {
        if (!hasL()) {
            return;
        }
        mActivity.getWindow().setStatusBarColor(color);
    }
}