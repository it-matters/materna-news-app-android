package de.materna.maternanews.ui;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import de.materna.maternanews.R;

public class EmptyStateFragment extends Fragment {

    public static final String IMAGE_EXTRA_KEY = "de.materna.maternanews.IMAGE_EXTRA_KEY";
    public static final String TEXT_EXTRA_KEY = "de.materna.maternanews.TEXT_EXTRA_KEY";

    @StringRes
    private int mTitle;
    @DrawableRes
    private int mImage;

    private Callback mCallback;

    public EmptyStateFragment() {
    }

    /**
     * Creates new instance of EmptyStateFragment.
     * @param image The image resource being displayed.
     * @param text The text resource being displayed.
     * @return EmptyStateFragment instance.
     */
    public static EmptyStateFragment create(@DrawableRes int image, @StringRes int text) {
        Bundle args = new Bundle();
        args.putInt(IMAGE_EXTRA_KEY, image);
        args.putInt(TEXT_EXTRA_KEY, text);
        EmptyStateFragment fragment = new EmptyStateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mImage = args.getInt(IMAGE_EXTRA_KEY);
        mTitle = args.getInt(TEXT_EXTRA_KEY);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_empty_state, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView emptyImage = (ImageView) view.findViewById(R.id.empty_image);
        TextView emptyText = (TextView) view.findViewById(android.R.id.empty);
        Button retryBtn = (Button) view.findViewById(R.id.retry_btn);

        emptyImage.setImageResource(mImage);
        emptyText.setText(mTitle);
        retryBtn.setOnClickListener(v -> mCallback.onRetry());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (!(context instanceof Callback)) {
            throw new IllegalStateException(context +
                    " must implement EmptyStateFragment.Callback");
        }
        mCallback = (Callback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    public interface Callback {

        /**
         * Retry action callback.
         */
        void onRetry();
    }
}