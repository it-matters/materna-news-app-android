package de.materna.maternanews.news;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.materna.maternanews.R;
import de.materna.maternanews.app.Constants;
import de.materna.maternanews.model.TopNewsEntry;
import de.materna.maternanews.ui.BaseActivity;
import de.materna.maternanews.ui.TopNewsAdapter;


public class TopNewsActivity extends BaseActivity {

    private RecyclerView mRecyclerView;

    private TopNewsAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_news);
        overridePendingTransition(0, 0);

        mAdapter = new TopNewsAdapter(this, new ArrayList<>(),
                this::dispatchShowNewsDetailIntent);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mAdapter.setData(savedInstanceState.getParcelableArrayList(
                    Constants.EXTRA_KEY_TOP_NEWS_LIST));
        } else {
            getDemoNews();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(Constants.EXTRA_KEY_TOP_NEWS_LIST,
                (ArrayList<TopNewsEntry>) mAdapter.getData());
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_TOP_NEWS;
    }

    private void dispatchShowNewsDetailIntent(int position) {

    }

    private List<TopNewsEntry> parseData(String data) {
        Gson gson = new Gson();
        Type type = new TypeToken<Collection<TopNewsEntry>>() {
        }.getType();
        List<TopNewsEntry> newsEntries = gson.fromJson(data, type);
        return newsEntries != null ? newsEntries : Collections.emptyList();
    }

    private String getDemoData() {
        String json;
        try {
            InputStream is = getAssets().open("topnews.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            Log.d("MATERNA", json);
        } catch (IOException ex) {
            return null;
        }
        return json;
    }

    private void getDemoNews() {
        mAdapter.setData(parseData(getDemoData()));
    }
}