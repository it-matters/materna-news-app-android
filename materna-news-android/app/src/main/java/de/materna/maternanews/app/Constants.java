package de.materna.maternanews.app;


public class Constants {

    public static final String EXTRA_KEY_NEWS_LIST = "de.materna.maternanews.EXTRA_KEY_NEWS_LIST";
    public static final String EXTRA_KEY_TOP_NEWS_LIST = "de.materna.maternanews.EXTRA_KEY_TOP_NEWS_LIST";
    public static final String EXTRA_KEY_NEWS_TITLE = "de.materna.maternanews.EXTRA_KEY_NEWS_TITLE";
    public static final String EXTRA_KEY_NEWS_AUTHOR = "de.materna.maternanews.EXTRA_KEY_NEWS_AUTHOR";
    public static final String EXTRA_KEY_NEWS_CONTENT = "de.materna.maternanews.EXTRA_KEY_NEWS_CONTENT";
}
