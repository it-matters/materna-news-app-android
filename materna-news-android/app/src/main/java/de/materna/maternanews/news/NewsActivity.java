package de.materna.maternanews.news;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import de.materna.maternanews.R;
import de.materna.maternanews.ui.BaseActivity;
import de.materna.maternanews.ui.EmptyStateFragment;
import de.materna.maternanews.util.ConnectivityUtils;

public class NewsActivity extends BaseActivity
        implements EmptyStateFragment.Callback {

    private Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        overridePendingTransition(0, 0);

        if (savedInstanceState == null) {
            mFragment = ConnectivityUtils.isConnectedOrConnecting(this) ? NewsFragment.create()
                    : EmptyStateFragment.create(R.drawable.ic_no_connection,
                    R.string.description_offline);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, mFragment, "newsActivityFragment").commit();
        } else {
            mFragment = getSupportFragmentManager().findFragmentByTag("newsActivityFragment");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mFragment, "newsActivityFragment").commit();
        }
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_NEWS;
    }

    @Override
    public void onRetry() {
        if ((mFragment instanceof EmptyStateFragment) &&
                ConnectivityUtils.isConnectedOrConnecting(this)) {
            mFragment = NewsFragment.create();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mFragment, "newsActivityFragment").commit();
        }
    }
}