package de.materna.maternanews.util;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Utilities and constants related to app settings.
 */
public class SettingsUtils {

    /**
     * Boolean indicating whether the app has performed the first run flow.
     */
    public static final String PREF_FIRST_RUN_COMPLETE =
            "com.materna.maternanews.PREF_FIRST_RUN_COMPLETE";

    /**
     * Return true if the first-app-run-activities have already been executed.
     *
     * @param context Context to be used to lookup the {@link android.content.SharedPreferences}.
     */
    public static boolean isFirstRunProcessComplete(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_FIRST_RUN_COMPLETE, false);
    }

    /**
     * Mark {@code newValue whether} this is the first time the first-app-run-processes have run.
     * Managed by {@link de.materna.maternanews.ui.BaseActivity the}
     * {@link de.materna.maternanews.ui.BaseActivity two} base activities.
     *
     * @param context  Context to be used to edit the {@link android.content.SharedPreferences}.
     * @param newValue New value that will be set.
     */
    public static void markFirstRunProcessesDone(final Context context, boolean newValue) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putBoolean(PREF_FIRST_RUN_COMPLETE, newValue).apply();
    }
}
