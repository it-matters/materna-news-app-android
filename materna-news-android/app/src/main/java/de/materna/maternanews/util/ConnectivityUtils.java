package de.materna.maternanews.util;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Utility class for connectivity.
 */
public class ConnectivityUtils {

    /**
     * Indicates whether network connectivity exists or is in the process of being established.
     * @param context
     * @return <code>true</code> if connectivity exists <code>false</code> otherwise.
     */
    public static boolean isConnectedOrConnecting(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getApplicationContext()
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnectedOrConnecting());
    }
}