package de.materna.maternanews.app;


import android.support.multidex.MultiDexApplication;

import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.mobile.push.PushManager;


public class Application extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        initializeApplication();
    }

    private void initializeApplication() {
        AWSMobileClient.initializeMobileClientIfNecessary(getApplicationContext());

        PushManager.setPushStateListener((pushManager, isEnabled) -> {
        });
    }
}
