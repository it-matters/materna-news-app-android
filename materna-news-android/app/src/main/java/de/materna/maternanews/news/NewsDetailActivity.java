package de.materna.maternanews.news;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import de.materna.maternanews.R;
import de.materna.maternanews.app.Constants;
import de.materna.maternanews.ui.BaseActivity;

public class NewsDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        overridePendingTransition(0, 0);

        Toolbar toolbar = getActionBarToolbar();
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_up);
            toolbar.setNavigationOnClickListener(view -> finish());
        }

        TextView titleView = (TextView) findViewById(R.id.title);
        TextView subtitleView = (TextView) findViewById(R.id.subtitle);
        WebView contentView = (WebView) findViewById(R.id.content);

        titleView.setText(getIntent().getStringExtra(Constants.EXTRA_KEY_NEWS_TITLE));
        subtitleView.setText(getIntent().getStringExtra(Constants.EXTRA_KEY_NEWS_AUTHOR));
        contentView.setScrollbarFadingEnabled(true);
        contentView.loadDataWithBaseURL(null,
                getIntent().getStringExtra(Constants.EXTRA_KEY_NEWS_CONTENT), "text/html",
                "charset=UTF-8", null);
    }
}