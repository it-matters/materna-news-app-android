package de.materna.maternanews.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsEntry implements Parcelable {

    @Expose
    @SerializedName("articleId")
    private String mArticleId;
    @Expose
    @SerializedName("userId")
    private String mUserId;
    @Expose
    @SerializedName("author")
    private String mAuthor;
    @Expose
    @SerializedName("title")
    private String mTitle;
    @Expose
    @SerializedName("content")
    private String mContent;

    public NewsEntry() {
    }

    private NewsEntry(Parcel source) {
        mArticleId = source.readString();
        mUserId = source.readString();
        mAuthor = source.readString();
        mTitle = source.readString();
        mContent = source.readString();
    }

    public String getArticleId() {
        return mArticleId;
    }

    public void setArticleId(String articleId) {
        mArticleId = articleId;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flag) {
        parcel.writeString(mArticleId);
        parcel.writeString(mUserId);
        parcel.writeString(mAuthor);
        parcel.writeString(mTitle);
        parcel.writeString(mContent);
    }

    public static final Parcelable.Creator<NewsEntry> CREATOR =
            new Parcelable.Creator<NewsEntry>() {


                @Override
                public NewsEntry createFromParcel(Parcel parcel) {
                    return new NewsEntry(parcel);
                }

                @Override
                public NewsEntry[] newArray(int size) {
                    return new NewsEntry[size];
                }
            };
}