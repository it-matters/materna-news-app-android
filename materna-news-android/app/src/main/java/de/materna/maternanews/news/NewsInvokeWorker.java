package de.materna.maternanews.news;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.amazonaws.mobile.AWSMobileClient;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

import de.materna.maternanews.BuildConfig;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

/**
 * Headless fragment manages a single background task and retains itself across configuration
 * changes.
 */
public final class NewsInvokeWorker extends Fragment {

    private Callbacks mCallbacks;
    private NewsInvokeTask mTask;

    private boolean mIsRunning;

    public NewsInvokeWorker() {
    }

    public static NewsInvokeWorker create() {
        return new NewsInvokeWorker();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (!(getTargetFragment() instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Target fragment must implement the NewsInvokeWorker.Callbacks.");
        }
        mCallbacks = (Callbacks) getTargetFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    /**
     * Starts performing the background work.
     */
    public void start() {
        if (!mIsRunning) {
            mTask = new NewsInvokeTask();
            mTask.executeOnExecutor(THREAD_POOL_EXECUTOR);
            mIsRunning = true;
        }
    }

    /**
     * Cancels the background task.
     */
    public void cancel() {
        if (mIsRunning) {
            mTask.cancel(false);
            mTask = null;
            mIsRunning = false;
        }
    }

    public boolean isRunning() {
        return mIsRunning;
    }

    public interface Callbacks {

        void onPreExecute();

        void onProgressUpdate(int progress);

        void onCanceled();

        void onPostExecute(Object object);
    }

    /**
     * Task that performs background work and invokes specific
     * <a href="http://docs.aws.amazon.com/lambda/latest/dg/versioning-aliases.html">
     * AWS Lambda Function</a>
     */
    private class NewsInvokeTask extends AsyncTask<Void, Integer, InvokeResult> {

        private static final String FUNCTION_NAME = "maternanewsapp-dev-getNews";
        private String mRequestPayload = "[]";

        private final Charset CHARSET_UTF8 =
                Charset.forName("UTF-8");

        private final CharsetEncoder ENCODER = CHARSET_UTF8.newEncoder();
        private final CharsetDecoder DECODER = CHARSET_UTF8.newDecoder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mCallbacks.onPreExecute();
            mIsRunning = true;
        }

        @Override
        protected InvokeResult doInBackground(Void... voids) {
            try {
                final ByteBuffer payload =
                        ENCODER.encode(CharBuffer.wrap(mRequestPayload));

                final InvokeRequest invokeRequest =
                        new InvokeRequest()
                                .withFunctionName(FUNCTION_NAME)
                                .withInvocationType(InvocationType.RequestResponse)
                                .withPayload(payload);
                return AWSMobileClient
                        .defaultMobileClient()
                        .getCloudFunctionClient()
                        .invoke(invokeRequest);

            } catch (final Exception exception) {
                final InvokeResult result = new InvokeResult();
                result.setStatusCode(500);
                result.setFunctionError(exception.getMessage());
                return result;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mCallbacks.onProgressUpdate(values[0]);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mCallbacks.onCanceled();
            mIsRunning = false;
        }

        @Override
        protected void onPostExecute(InvokeResult result) {
            super.onPostExecute(result);
            mTask = null;
            try {
                final ByteBuffer resultPayloadBuffer = result.getPayload();
                final String resultPayload = DECODER.decode(resultPayloadBuffer).toString();
                mCallbacks.onPostExecute(resultPayload);
            } catch (final Exception exception) {
                mCallbacks.onPostExecute(null);
                if (BuildConfig.DEBUG) {
                    Log.d(NewsInvokeWorker.class.getName(), exception.toString() + "onPostExecute");
                }
            } finally {
                mIsRunning = false;
            }
        }
    }
}