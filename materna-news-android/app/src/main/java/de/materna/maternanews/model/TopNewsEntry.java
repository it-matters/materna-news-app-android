package de.materna.maternanews.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopNewsEntry implements Parcelable {

    @Expose
    @SerializedName("title")
    private String mTitle;
    @Expose
    @SerializedName("text")
    private String mContent;
    @Expose
    @SerializedName("base64Binary")
    private String mImageBinary;

    public TopNewsEntry() {
    }

    private TopNewsEntry(Parcel source) {
        mTitle = source.readString();
        mContent = source.readString();
        mImageBinary = source.readString();
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public String getImageBinary() {
        return mImageBinary;
    }

    public void setImageBinary(String imageBinary) {
        mImageBinary = imageBinary;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flag) {
        parcel.writeString(mTitle);
        parcel.writeString(mContent);
        parcel.writeString(mImageBinary);
    }

    public static final Parcelable.Creator<TopNewsEntry> CREATOR =
            new Parcelable.Creator<TopNewsEntry>() {

                @Override
                public TopNewsEntry createFromParcel(Parcel parcel) {
                    return new TopNewsEntry(parcel);
                }

                @Override
                public TopNewsEntry[] newArray(int size) {
                    return new TopNewsEntry[size];
                }
            };
}