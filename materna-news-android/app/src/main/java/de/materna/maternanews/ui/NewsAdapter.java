package de.materna.maternanews.ui;


import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.materna.maternanews.R;
import de.materna.maternanews.model.NewsEntry;

public final class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private List<NewsEntry> mData;
    private OnItemClickCallback mCallback;

    public NewsAdapter(List<NewsEntry> data, OnItemClickCallback callback) {
        mData = data;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_news, parent, false);
        return new ViewHolder(itemView, mCallback);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NewsEntry item = getItem(position);
        holder.mTitle.setText(item != null ? item.getTitle() : null);
        holder.mAuthor.setText(item != null ? item.getAuthor() : null);
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    @Nullable
    public NewsEntry getItem(int position) {
        return mData != null && mData.size() > 0 ? mData.get(position) : null;
    }

    public void setData(List<NewsEntry> data) {
        if (mData != null) {
            mData.clear();
            mData.addAll(data);
            notifyDataSetChanged();
        }
    }

    public List<NewsEntry> getData() {
        return mData;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTitle;
        TextView mAuthor;

        private OnItemClickCallback mCallback;

        ViewHolder(View itemView, OnItemClickCallback callback) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.title);
            mAuthor = (TextView) itemView.findViewById(R.id.author);
            mCallback = callback;

            itemView.setOnClickListener(view -> mCallback.onItemClick(getAdapterPosition()));
        }
    }

    @FunctionalInterface
    public interface OnItemClickCallback {

        void onItemClick(int position);
    }
}
