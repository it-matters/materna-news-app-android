package de.materna.maternanews.news;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.materna.maternanews.R;
import de.materna.maternanews.app.Constants;
import de.materna.maternanews.model.NewsEntry;
import de.materna.maternanews.ui.NewsAdapter;

public class NewsFragment extends Fragment implements NewsInvokeWorker.Callbacks {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private TextView mEmptyView;

    private NewsAdapter mAdapter;
    private NewsInvokeWorker mWorkerFragment;

    public NewsFragment() {
    }

    public static NewsFragment create() {
        return new NewsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new NewsAdapter(new ArrayList<>(), this::dispatchShowNewsDetailIntent);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mEmptyView = (TextView) view.findViewById(android.R.id.empty);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(),
                DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentManager manager = getActivity().getSupportFragmentManager();
        mWorkerFragment = (NewsInvokeWorker) manager.findFragmentByTag(
                NewsInvokeWorker.class.getSimpleName());

        if (mWorkerFragment == null) {
            mWorkerFragment = NewsInvokeWorker.create();
            mWorkerFragment.setTargetFragment(this, 100);
            manager.beginTransaction().add(mWorkerFragment,
                    NewsInvokeWorker.class.getSimpleName()).commit();
        }

        if (savedInstanceState != null) {
            mAdapter.setData(savedInstanceState.getParcelableArrayList(
                    Constants.EXTRA_KEY_NEWS_LIST));

            if (mWorkerFragment.isRunning() && mProgressBar.getVisibility() != View.VISIBLE) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_news, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refreshData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(Constants.EXTRA_KEY_NEWS_LIST,
                (ArrayList<NewsEntry>) mAdapter.getData());
    }

    @Override
    public void onPreExecute() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onProgressUpdate(int progress) {
        mProgressBar.setProgress(progress * mProgressBar.getMax() / 100);
    }


    @Override
    public void onCanceled() {
        mProgressBar.setProgress(0);
    }

    @Override
    public void onPostExecute(Object object) {
        mAdapter.setData(parseResponse((String) object));
        mProgressBar.setVisibility(View.GONE);
        updateUi();
    }

    /**
     * Parses network response.
     *
     * @param response The response being parsed.
     * @return List of {@link NewsEntry} objects.
     */
    private List<NewsEntry> parseResponse(String response) {
        Gson gson = new Gson();
        Type type = new TypeToken<Collection<NewsEntry>>() {
        }.getType();
        List<NewsEntry> newsEntries = gson.fromJson(response, type);
        return newsEntries != null ? newsEntries : Collections.emptyList();
    }

    /**
     * Starts {@link NewsDetailActivity}.
     *
     * @param position The position of item being clicked.
     */
    private void dispatchShowNewsDetailIntent(int position) {
        Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
        intent.putExtra(Constants.EXTRA_KEY_NEWS_TITLE, mAdapter.getItem(position).getTitle());
        intent.putExtra(Constants.EXTRA_KEY_NEWS_AUTHOR, mAdapter.getItem(position).getAuthor());
        intent.putExtra(Constants.EXTRA_KEY_NEWS_CONTENT, mAdapter.getItem(position).getContent());
        startActivity(intent);
    }

    private void refreshData() {
        mEmptyView.setVisibility(View.GONE);
        mWorkerFragment.start();
    }

    private void updateUi() {
        mEmptyView.setVisibility((mAdapter != null && mAdapter.getData().size() > 0) ?
                View.GONE : View.VISIBLE);
        mRecyclerView.setVisibility((mAdapter != null && mAdapter.getData().size() > 0) ?
                View.VISIBLE : View.GONE);
    }
}