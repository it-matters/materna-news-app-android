package de.materna.maternanews.ui;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.materna.maternanews.R;
import de.materna.maternanews.model.TopNewsEntry;
import de.materna.maternanews.util.ImageLoader;

public final class TopNewsAdapter extends RecyclerView.Adapter<TopNewsAdapter.ViewHolder> {

    private Context mContext;
    private List<TopNewsEntry> mData;
    private OnItemClickCallback mCallback;
    private ImageLoader mImageLoader;

    public TopNewsAdapter(Context context, List<TopNewsEntry> data, OnItemClickCallback callback) {
        mContext = context.getApplicationContext();
        mData = data;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(mContext);
        }
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_top_news, parent, false);
        return new ViewHolder(itemView, mCallback);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TopNewsEntry item = getItem(position);
        mImageLoader.loadImage(mContext, item != null ? item.getImageBinary() : null,
                holder.mImage);
        holder.mTitle.setText(item != null ? item.getTitle() : null);
        holder.mSubtitle.setText("Subhead");
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    @Nullable
    public TopNewsEntry getItem(int position) {
        return mData != null && mData.size() > 0 ? mData.get(position) : null;
    }

    public void setData(List<TopNewsEntry> data) {
        if (mData != null) {
            mData.clear();
            mData.addAll(data);
            notifyDataSetChanged();
        }
    }

    public List<TopNewsEntry> getData() {
        return mData;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mImage;
        TextView mTitle;
        TextView mSubtitle;
        Button mOpenArticleBtn;

        private OnItemClickCallback mCallback;

        public ViewHolder(View itemView, OnItemClickCallback callback) {
            super(itemView);

            mImage = (ImageView) itemView.findViewById(R.id.image);
            mTitle = (TextView) itemView.findViewById(R.id.title);
            mSubtitle = (TextView) itemView.findViewById(R.id.subtitle);
            mOpenArticleBtn = (Button) itemView.findViewById(R.id.open_article_btn);
            mCallback = callback;

            mOpenArticleBtn.setOnClickListener(view ->
                    mCallback.onItemClick(getAdapterPosition()));
        }
    }

    @FunctionalInterface
    public interface OnItemClickCallback {

        void onItemClick(int position);
    }
}